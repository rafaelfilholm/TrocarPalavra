onload = function (){
    document.getElementById("Alterar").onclick = function() {
        texto = document.getElementById("texto").value;
        palavra = document.getElementById("palavra").value;
        novaPalavra = document.getElementById("novaPalavra").value;
        novoTextoCampo = document.getElementById("novoTexto");

        if (texto == null || texto == ""|| palavra == "" || palavra == "") {
            alert("Por Favor, Digite o Texto e o Delimitador!");
            return false;
        } else{
            novoTexto = texto.split(palavra).join(novaPalavra);
            novoTextoCampo.value = novoTexto; 
        }
    };
};